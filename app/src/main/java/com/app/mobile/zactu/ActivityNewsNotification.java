package com.app.mobile.fashionable;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobile.fashionable.adapter.SliderAdapter;
import com.app.mobile.fashionable.controller.App;
import com.app.mobile.fashionable.model.Comment;
import com.app.mobile.fashionable.model.News;
import com.app.mobile.fashionable.model.NewsResp;
import com.app.mobile.fashionable.model.User;
import com.app.mobile.fashionable.model.VenteModel;
import com.app.mobile.fashionable.rest.PostService;
import com.app.mobile.fashionable.tools.Utilities;
import com.app.mobile.fashionable.viewholder.CommentViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.CallbackManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class ActivityNewsNotification extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, RewardedVideoAdListener {
    public static final String EXTRA_OBJC = "com.app.mobile.fashionable.EXTRA_OBJC";

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private FloatingActionButton fab;
    private TextView titre0;
    private TextView vente0;
    private TextView titre1;
    private TextView vente1;
    private TextView titre2;
    private TextView vente2;
    private TextView titre3;
    private TextView vente3;
    private ProgressBar progressbar;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    private static DatabaseReference mCommentsReference;
    static Map<String, Object> commentUpdates = new HashMap<String, Object>();
    public static CommentAdapter adapter;
    private Button btn_send;
    private EditText et_content;
    private RecyclerView listview;
    private News newsPost;
    Dialog dialog = null;
    private FirebaseAuth mAuth;
    private LinearLayout signInGoogleButton;
    private ImageView mPasserField;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    String thisUserId;
    String idpost;
    private static final int RC_SIGN_IN = 9001;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        parent_view = findViewById(android.R.id.content);
        idpost = (String) getIntent().getSerializableExtra(EXTRA_OBJC);
        initToolbar();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        titre0 = (TextView) findViewById(R.id.titre0);
        vente0 = (TextView) findViewById(R.id.vente0);
        titre1 = (TextView) findViewById(R.id.titre1);
        vente1 = (TextView) findViewById(R.id.vente1);
        titre2 = (TextView) findViewById(R.id.titre2);
        vente2 = (TextView) findViewById(R.id.vente2);
        titre3 = (TextView) findViewById(R.id.titre3);
        vente3 = (TextView) findViewById(R.id.vente3);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        loadRewardedVideoAd();

        getNewsInfos(idpost);

        iniComponen();
        listview.setLayoutManager(new LinearLayoutManager(this));
        FirebaseApp.initializeApp(this);
        mCommentsReference = FirebaseDatabase.getInstance().getReference()
                .child("comments").child(idpost);

        adapter = new CommentAdapter(this, mCommentsReference);
        listview.setAdapter(adapter);
        init();
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder().build());
    }

    @Override
    public void onRewarded(RewardItem reward) {
        Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " +
                reward.getAmount(), Toast.LENGTH_SHORT).show();
        // Reward the user.
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Toast.makeText(this, "onRewardedVideoAdLeftApplication",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoStarted() {
        Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    private void init() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SliderAdapter(getApplicationContext() ,newsPost.getUrls()));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (currentPage == newsPost.getUrls().size()) {
//                    currentPage = 0;
//                }
//                mPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 5000, 5000);
    }

    private Callback<NewsResp> callback = new Callback<NewsResp>() {

        @Override
        public void onResponse(Call<NewsResp> call, Response<NewsResp> res) {
            int code = res.code();
            if (code == 200) {
                initViews(res.body().getPost());
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }

            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<NewsResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void getNewsInfos(String postid) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            progressbar.setVisibility(View.GONE);
            Utilities.showMessage(null, getString(R.string.no_internet), getApplicationContext());
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<NewsResp> operations;
        operations = postService.post(postid);
        operations.enqueue(callback);
    }

    private void initViews(final News news) {
        newsPost = news;
        ((TextView) findViewById(R.id.title)).setText(news.getPosttitre());
        ((TextView) findViewById(R.id.content)).setText(news.getPosttexte());
        ((TextView) findViewById(R.id.date)).setText(news.getDate());
        ((TextView) findViewById(R.id.channel)).setText(news.getCategory());
        for(String name : news.getUrls()){
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image(name)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

//            imageSlider.addSlider(textSliderView);
        }
//        imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
//        imageSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator2));
//        imageSlider.setCustomAnimation(new DescriptionAnimation());
//        if (news.getUrls().size() > 1){
//            imageSlider.setDuration(4000);
//        }else {
//            imageSlider.stopAutoCycle();
//            imageSlider.setDuration(600000);
//        }
//        imageSlider.addOnPageChangeListener(this);
        for (int i = 0; i < news.getVentes().size(); i++) {
            VenteModel venteModel = news.getVentes().get(i);
            if (i == 0){
                titre0.setVisibility(View.VISIBLE);
                vente0.setVisibility(View.VISIBLE);
                titre0.setText(venteModel.getTitre());
                vente0.setText(venteModel.getUrl());
                vente0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vente0.getText().toString()));
                        startActivity(browserIntent);
                    }
                });
            }
            if (i == 1){
                titre1.setVisibility(View.VISIBLE);
                vente1.setVisibility(View.VISIBLE);
                titre1.setText(venteModel.getTitre());
                vente1.setText(venteModel.getUrl());
                vente1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vente1.getText().toString()));
                        startActivity(browserIntent);
                    }
                });
            }

            if (i == 2){
                titre2.setVisibility(View.VISIBLE);
                vente2.setVisibility(View.VISIBLE);
                titre2.setText(venteModel.getTitre());
                vente2.setText(venteModel.getUrl());
                vente2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vente2.getText().toString()));
                        startActivity(browserIntent);
                    }
                });
            }

            if (i == 3){
                titre3.setVisibility(View.VISIBLE);
                vente3.setVisibility(View.VISIBLE);
                titre3.setText(venteModel.getTitre());
                vente3.setText(venteModel.getUrl());
                vente3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vente3.getText().toString()));
                        startActivity(browserIntent);
                    }
                });
            }
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.isSaved(news)) {
                    App.removeSaved(news);
                    Snackbar.make(parent_view, "News removed from favorites", Snackbar.LENGTH_SHORT).show();
                } else {
                    App.addSaved(news);
                    Snackbar.make(parent_view, "News added to favorites", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }else{
            Snackbar.make(parent_view, item.getTitle() + " clicked", Snackbar.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
//        imageSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.e("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    public void bindView() {
        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
    }

    public void iniComponen() {
        listview = (RecyclerView) findViewById(R.id.recyclerView);
        btn_send = (Button) findViewById(R.id.btn_send);
        et_content = (EditText) findViewById(R.id.text_content_message);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postComment(et_content.getText().toString());
                et_content.setText("");
                bindView();
                hideKeyboard();
            }
        });
        et_content.addTextChangedListener(contentWatcher);
        if (et_content.length() == 0) {
            btn_send.setEnabled(false);
        }
        hideKeyboard();
        et_content.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (getUid() == null){
                    dialogLogin();
                }else {
                    et_content.setFocusableInTouchMode(true);
                }
            }
        });
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
            if (etd.toString().trim().length() == 0) {
                btn_send.setEnabled(false);
            } else {
                btn_send.setEnabled(true);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };

    private void postComment(final String content) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy");
        String formattedDate = df.format(c.getTime());

        String commentText = content;
        Comment comment = new Comment(getUid(), FirebaseAuth.getInstance().getCurrentUser().getDisplayName(), FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString(), commentText, formattedDate);
        mCommentsReference.push().setValue(comment);
        et_content.setText(null);
        listview.post(new Runnable() {
            @Override
            public void run() {
                listview.smoothScrollToPosition(adapter.getItemCount());
            }
        });
    }

    private void dialogLogin() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_login);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mPasserField = (ImageView) dialog.findViewById(R.id.closeLogin);
        signInGoogleButton = (LinearLayout) dialog.findViewById(R.id.btn_compte_google);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null){
                        thisUserId = user.getUid();
                        onAuthSuccessKnownUser(user);
                    }
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        callbackManager = CallbackManager.Factory.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        signInGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        (this).startActivityForResult(signInIntent, RC_SIGN_IN);
        dialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                Log.d("SIGN IN GOOGLE OK :", account.getDisplayName());
                Toast.makeText(getApplicationContext(), "Connexion réussie!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Echec Connexion! Veuillez ressayer plus tard.", Toast.LENGTH_SHORT).show();
                Log.d("SIGN IN GOOGLE OK :", "FAILED");
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        showProgressDialog();
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        BaseApp.authenticatedUser = new User(acct.getDisplayName(), acct.getEmail(), acct.getFamilyName(), acct.getServerAuthCode());
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        final User userG = new User(acct.getDisplayName().toLowerCase(), acct.getEmail(), acct.getPhotoUrl().toString(), FirebaseInstanceId.getInstance().getToken());
                        if (thisUserId != null){
                            Toast.makeText(getApplicationContext(), "Connected!",
                                    Toast.LENGTH_SHORT).show();
                            BaseApp.database.getReference("users").child(thisUserId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User user1 = dataSnapshot.getValue(User.class);
                                    if (user1 == null){
                                        BaseApp.database.getReference("users").child(thisUserId).setValue(userG);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Echec connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void onAuthSuccessKnownUser(FirebaseUser user) {
        dialog.hide();
    }

    public class CommentAdapter extends RecyclerView.Adapter<CommentViewHolder> implements View.OnClickListener {

        private static final String TAG = "ActivityChatDetails";
        private Context mContext;
        private List<String> mCommentIds = new ArrayList<>();
        private List<Comment> mComments = new ArrayList<>();
        private SparseBooleanArray selectedItems = new SparseBooleanArray();

        public CommentAdapter(final Context context, DatabaseReference ref) {
            mContext = context;

            ChildEventListener childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                    if ((dataSnapshot.getKey()).equals("numcomments")) {
                        System.out.println("ok");
                    } else {
                        Comment comment = dataSnapshot.getValue(Comment.class);
                        mCommentIds.add(dataSnapshot.getKey());
                        mComments.add(comment);
                        notifyItemInserted(mComments.size() - 1);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

                    if ((dataSnapshot.getKey()).equals("numcomments")) {
                        System.out.println("ok");
                    } else {
                        Comment newComment = dataSnapshot.getValue(Comment.class);
                        String commentKey = dataSnapshot.getKey();

                        int commentIndex = mCommentIds.indexOf(commentKey);
                        if (commentIndex > -1) {
                            mComments.set(commentIndex, newComment);

                            notifyItemChanged(commentIndex);
                        } else {
                            Log.w(TAG, "onChildChanged:unknown_child:" + commentKey);
                        }
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                    String commentKey = dataSnapshot.getKey();

                    int commentIndex = mCommentIds.indexOf(commentKey);
                    if (commentIndex > -1) {
                        mCommentIds.remove(commentIndex);
                        mComments.remove(commentIndex);

                        notifyItemRemoved(commentIndex);
                    } else {
                        Log.w(TAG, "onChildRemoved:unknown_child:" + commentKey);
                    }
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                    Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                    Toast.makeText(mContext, "Failed to load comments.",
                            Toast.LENGTH_SHORT).show();
                }
            };
            ref.addChildEventListener(childEventListener);
        }

        @Override
        public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.row_message_details, parent, false);
            return new CommentViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final CommentViewHolder holder, final int position) {
            Comment comment = mComments.get(position);
            holder.authorView.setText(comment.author);
            holder.bodyView.setText(comment.text);
            holder.dateView.setText(comment.date);
            holder.cmtBackground.setSelected(selectedItems.get(position, false));
            if (!comment.uid.equals(getUid())) {
                holder.cmtBackground.setOnLongClickListener(null);
            } else {
                holder.cmtBackground.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        // TODO Auto-generated method stub
                        PopupMenu popup = new PopupMenu(mContext, holder.cmtBackground);
                        popup.inflate(R.menu.menu_options);
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.item_supprimer:
                                        DatabaseReference mCommentsRef = FirebaseDatabase.getInstance().getReference()
                                                .child("comments").child(newsPost.getId()).child(mCommentIds.get(holder.getAdapterPosition()));
                                        mCommentsRef.removeValue();
                                        FirebaseDatabase.getInstance().getReference()
                                                .child("comments").child(newsPost.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                int nbComments;
                                                if (dataSnapshot.child("numcomments").getValue() != null) {
                                                    nbComments = (int) (long) dataSnapshot.child("numcomments").getValue() - 1;
                                                    commentUpdates.put("numcomments", nbComments);
                                                    mCommentsReference.child("numcomments").setValue(nbComments);
                                                } else {
                                                    nbComments = 0;
                                                    commentUpdates.put("numcomments", nbComments);
                                                    mCommentsReference.setValue(commentUpdates);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                        break;
                                    case R.id.item_modifier:
                                        Intent intent = new Intent(mContext, UpdateCommentActivity.class);
                                        intent.putExtra("commentId", mCommentIds.get(holder.getAdapterPosition()));
                                        intent.putExtra("commentBody", holder.bodyView.getText());
                                        intent.putExtra("postKey", newsPost.getId());
                                        startActivity(intent);
                                        break;
                                }
                                return false;
                            }
                        });
                        popup.show();

                        return true;
                    }
                });
            }

            if (comment.authorPp != "" || comment.authorPp != null) {
                Glide.with(holder.bodyView.getContext()).load(comment.authorPp)
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(150, 150)
                        .into(new BitmapImageViewTarget(holder.authorPpView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.authorPpView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            }

            if (comment.authorPp == null || comment.authorPp.isEmpty()) {
                Glide.with(holder.authorPpView.getContext()).load(R.drawable.ic_audiotrack_dark)
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(150, 150)
                        .into(new BitmapImageViewTarget(holder.authorPpView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.authorPpView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            }
        }

        @Override
        public int getItemCount() {
            return mComments.size();
        }

        @Override
        public void onClick(View v) {
        }

        public String getUid() {
            if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                return null;
            } else {
                return FirebaseAuth.getInstance().getCurrentUser().getUid();
            }
        }
    }
}
