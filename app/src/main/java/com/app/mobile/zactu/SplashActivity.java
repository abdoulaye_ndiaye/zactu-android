package com.app.mobile.fashionable;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.app.mobile.fashionable.tools.ForceUpdateChecker;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by ghambyte on 20/11/2017 at 12:45.
 * Project name : BGFIMobile
 */

public class SplashActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener {

    private final int SPLASH_DISPLAY_LENGHT = 3000;
    private ImageView imageView = null;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        requestNewInterstitial();
        imageView = (ImageView) findViewById(R.id.image);

        Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_out);
        imageView.startAnimation(animation);

        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                //mInterstitialAd.loadAd(new AdRequest.Builder().build());
                requestNewInterstitial();
                Intent mainIntent = new Intent(SplashActivity.this,ActivityMain.class);
                SplashActivity.this.startActivity(mainIntent);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        Intent mainIntent = new Intent(SplashActivity.this,ActivityMain.class);

                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            SplashActivity.this.startActivity(mainIntent);
                        }
                        SplashActivity.this.finish();
                    }
                }, SPLASH_DISPLAY_LENGHT);
            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

//        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Nouvelle version disponible")
                .setMessage("Veuillez mettre à jour votre application pour continuer.")
                .setPositiveButton("Mettre à jour",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("Non, merci",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();

    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
