package com.app.mobile.fashionable;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.mobile.fashionable.model.Comment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

/**
 * Created by ghambyte on 21/12/2016.
 */

public class UpdateCommentActivity extends BaseActivity{

    private FirebaseAuth mAuth;
//    private DatabaseReference mDatabase;
    public EditText commentEdit;
    public Button buttonUpdate;
    public Button buttonAnnuler;
    String key = null;
    private static final String TAG = "UPDATE";
    String commentId = null;
    String commentBody = null;
    String postKey = null;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_comment);
        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar();
        mToolBar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_vector_close));
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        commentEdit = (EditText) findViewById(R.id.fieldCommentBody);
        buttonUpdate = (Button) findViewById(R.id.updateComment);
        buttonAnnuler = (Button) findViewById(R.id.annulerUodate);
        mAuth = FirebaseAuth.getInstance();
        Bundle extras = getIntent().getExtras();
        commentBody = extras.getString("commentBody");
        commentId = extras.getString("commentId");
        postKey = extras.getString("postKey");
        commentEdit.setText(commentBody);
        buttonUpdate.setOnClickListener(updateHandler);
        buttonAnnuler.setOnClickListener(updateAnnuleHandler);
        commentEdit.requestFocus();
        commentEdit.setSelection(commentEdit.getText().length());
        commentEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (commentEdit.getText().toString().isEmpty()){
                    buttonUpdate.setClickable(false);
                    buttonUpdate.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                }else {
                    buttonUpdate.setClickable(true);
                    buttonUpdate.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {
                Log.e("TextWatcherTest", "afterTextChanged:\t" +s.toString());
            }
        });
    }

    View.OnClickListener updateHandler = new View.OnClickListener() {
        public void onClick(View v) {
            updateComment();
        }
    };

    View.OnClickListener updateAnnuleHandler = new View.OnClickListener() {
        public void onClick(View v) {
//            onBackPressed();
            finish();
        }
    };

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Modifier le commentaire");
    }

    public void updateComment(){
        final DatabaseReference globalPostRef = FirebaseDatabase.getInstance().getReference("comments").child(postKey).child(commentId);
        globalPostRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Comment comment = mutableData.getValue(Comment.class);
                if (comment == null) {
                    return Transaction.success(mutableData);
                }
                comment.text = commentEdit.getText().toString();
                mutableData.setValue(comment);
                finish();
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
