package com.app.mobile.fashionable.controller;

import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;

import com.app.mobile.fashionable.BaseApp;
import com.app.mobile.fashionable.BuildConfig;
import com.app.mobile.fashionable.model.News;
import com.google.firebase.FirebaseApp;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ghambyte on 25/04/2017.
 */

public class App extends MultiDexApplication {

    public static final Bus BUS = new Bus(ThreadEnforcer.ANY);

    private static App mInstance;

    private static final String TAG = App.class.getSimpleName();

    private static List<News> saved = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        mInstance = this;
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeathOnNetwork()
                .build());

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder().addNetworkInterceptor(interceptor).build();


        BaseApp.mobileAdapter = new Retrofit.Builder()
                .baseUrl(BuildConfig.URL_DEBUG)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
//        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // set in-app defaults

    }

    public static List<News> getSaved() {
        return saved;
    }

    public static void addSaved(News s) {
        saved.add(s);
    }
    public static void removeSaved(News s) {
        for (int i=0; i<saved.size(); i++){
            if(saved.get(i).getId().equalsIgnoreCase(s.getId())){
                saved.remove(i);
            }
        }
    }
    public static boolean isSaved(News s) {
        if (saved == null || saved.isEmpty()){
            return false;
        }
        for (News a : saved){
            if(a.getId().equalsIgnoreCase(s.getId())){
                return true;
            }
        }
        return false;
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

}
