package com.app.mobile.fashionable.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mobile.fashionable.ActivityRegister;
import com.app.mobile.fashionable.BuildConfig;
import com.app.mobile.fashionable.R;
import com.google.android.gms.ads.AdView;

public class FragmentSetting extends Fragment {

    View view;
    LinearLayout textViewShare;
    LinearLayout textViewNote;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting, null);
        textViewShare = (LinearLayout) view.findViewById(R.id.shareApp);
        textViewNote = (LinearLayout) view.findViewById(R.id.linearNote);
        textViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.str_share_app_text));
                intent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.chakamobile.dinero");
                String title = getResources().getString(R.string.share_via);
                Intent chooser = Intent.createChooser(intent, title);

                if (chooser.resolveActivity(getActivity().getPackageManager())
                        != null) {
                    startActivity(chooser);
                }
            }
        });
        textViewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=")));
                }
            }
        });
        // activate fragment menu
        setHasOptionsMenu(true);

        return view;
    }

}
