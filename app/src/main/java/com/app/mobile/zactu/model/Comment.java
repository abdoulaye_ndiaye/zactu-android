package com.app.mobile.fashionable.model;

import com.google.firebase.database.IgnoreExtraProperties;

// [START comment_class]
@IgnoreExtraProperties
public class Comment {

    public String uid;
    public String author;
    public String authorPp;
    public String text;
    public String date;

    public Comment() {
        // Default constructor required for calls to DataSnapshot.getValue(Comment.class)
    }

    public Comment(String uid, String text, String author) {
        this.uid = uid;
        this.text = text;
        this.author = author;
    }

    public Comment(String uid, String author, String authorPp, String text, String date) {
        this.uid = uid;
        this.author = author;
        this.authorPp = authorPp;
        this.text = text;
        this.date = date;
    }

}
// [END comment_class]
