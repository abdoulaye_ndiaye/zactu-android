package com.app.mobile.fashionable.model;

import java.io.Serializable;
import java.util.List;

public class News implements Serializable{
    private String title;
    private String more;
    private String date;
    private String url;
    private String id;
    private String posttitre;
    private String posttexte;
    private String datepost;
    private String category;
    List<String> urls;
    List<VenteModel> ventes;
    private int image;
    private String content;
    private Channel channel;

    public News() {
    }

    public News(String title, String date, int image, String content, Channel channel) {
        this.title = title;
        this.date = date;
        this.image = image;
        this.content = content;
        this.channel = channel;
    }

    public News(String title, String more, String url, String id, String date, int image, String content, Channel channel) {
        this.title = title;
        this.more = more;
        this.url = url;
        this.id = id;
        this.date = date;
        this.image = image;
        this.content = content;
        this.channel = channel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShort_content() {
        if(content.length()>100){
            return content.substring(0, 80)+"...";
        }
        return content+"...";
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public String getPosttitre() {
        return posttitre;
    }

    public void setPosttitre(String posttitre) {
        this.posttitre = posttitre;
    }

    public String getPosttexte() {
        return posttexte;
    }

    public void setPosttexte(String posttexte) {
        this.posttexte = posttexte;
    }

    public String getDatepost() {
        return datepost;
    }

    public void setDatepost(String datepost) {
        this.datepost = datepost;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<VenteModel> getVentes() {
        return ventes;
    }

    public void setVentes(List<VenteModel> ventes) {
        this.ventes = ventes;
    }
}
