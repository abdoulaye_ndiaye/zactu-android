package com.app.mobile.fashionable.model;

import java.io.Serializable;

/**
 * Created by ghambyte on 25/04/2017.
 */

public class PostItem implements Serializable {

    String id;

    String author;

    String authorid;

    String url;

    String category;

    String sexe;

    String more;

    String photouser;

    String pays;

    String telephone;

    String date;

    public PostItem() {
    }

    public PostItem(String category, String sexe) {
        this.category = category;
        this.sexe = sexe;
    }

    public PostItem(String authorId, String category, String sexe) {
        this.category = category;
        this.sexe = sexe;
        this.authorid = authorId;
    }

    public PostItem(String author, String authorid, String url, String category, String sexe, String more,
                    String photoUser, String pays, String telephone, String date) {
        this.author = author;
        this.authorid = authorid;
        this.url = url;
        this.category = category;
        this.sexe = sexe;
        this.more = more;
        this.photouser = photoUser;
        this.pays = pays;
        this.telephone = telephone;
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getPhotouser() {
        return photouser;
    }

    public void setPhotouser(String photouser) {
        this.photouser = photouser;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PostItem{" +
                "id='" + id + '\'' +
                ", author='" + author + '\'' +
                ", authorid='" + authorid + '\'' +
                ", url='" + url + '\'' +
                ", category='" + category + '\'' +
                ", sexe='" + sexe + '\'' +
                '}';
    }
}
