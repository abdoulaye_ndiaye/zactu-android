package com.app.mobile.fashionable.model;

import java.util.List;

/**
 * Created by ghambyte on 25/04/2017 at 22:19.
 * Project name : Modelic-V2
 */

public class PostResp {

    List<News> posts;

    public List<News> getPostItems() {
        return posts;
    }
}
