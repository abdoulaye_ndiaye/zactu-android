package com.app.mobile.fashionable.model;

/**
 * Created by ghambyte on 25/04/2017 at 17:25.
 * Project name : Modelic-V2
 */
public class ResponseBase {

    private int code;

    private String message;

    public ResponseBase(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponBase{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
