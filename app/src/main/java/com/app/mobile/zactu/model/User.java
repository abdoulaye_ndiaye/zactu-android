package com.app.mobile.fashionable.model;

import com.google.firebase.database.IgnoreExtraProperties;

// [START blog_user_class]
@IgnoreExtraProperties
public class User {

    public String username;
    public String email;
    public String pprofile;
    public String telProfile;
    public String address;
    public String more;
    public String token;
    public String site;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email, String pprofile, String token) {
        this.username = username;
        this.email = email;
        this.pprofile = pprofile;
        this.token = token;
    }

    public User(String username, String email, String pprofile, String tel, String address, String more, String token, String site) {
        this.username = username;
        this.email = email;
        this.pprofile = pprofile;
        this.telProfile = tel;
        this.address = address;
        this.more = more;
        this.token = token;
        this.site = site;
    }

}
// [END blog_user_class]
