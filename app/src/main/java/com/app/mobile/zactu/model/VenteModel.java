package com.app.mobile.fashionable.model;

import java.io.Serializable;

/**
 * Created by ghambyte on 16/11/2017 at 11:17.
 * Project name : BGFIMobile
 */

public class VenteModel implements Serializable{

    String titre;

    String url;

    public VenteModel() {
    }

    public VenteModel(String titre, String url) {
        this.titre = titre;
        this.url = url;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Vente{" +
                "titre='" + titre + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
