package com.app.mobile.fashionable.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.app.mobile.fashionable.ActivityMain;
import com.app.mobile.fashionable.BaseApp;
import com.app.mobile.fashionable.R;
import com.app.mobile.fashionable.model.PostItem;
import com.app.mobile.fashionable.model.ResponseBase;
import com.app.mobile.fashionable.model.User;
import com.app.mobile.fashionable.rest.PostService;
import com.app.mobile.fashionable.tools.Utilities;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Service to handle uploading files to Firebase Storage.
 */
public class MyUploadService extends MyBaseTaskService {

    private static final String TAG = "MyUploadService";
    private static final int NOTIF_ID_DOWNLOAD = 0;

    /** Intent Actions **/
    public static final String ACTION_UPLOAD = "action_upload";
    public static final String UPLOAD_COMPLETED = "upload_completed";
    public static final String UPLOAD_ERROR = "upload_error";

    /** Intent Extras **/
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_FILE_URI1 = "extra_file_uri1";
    public static final String EXTRA_FILE_URI2 = "extra_file_uri2";
    public static final String EXTRA_FILE_URI3 = "extra_file_uri3";
    public static final String EXTRA_FILE_URI4 = "extra_file_uri4";
    public static final String EXTRA_FILE_URI5 = "extra_file_uri5";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";

    String telTailleur;
    String description;
    String more;
    String sexe;
    String category;
    String key = FirebaseDatabase.getInstance().getReference("posts").push().getKey();
    ArrayList<Uri> arrayList = new ArrayList<Uri>();

    private StorageReference mStorageRef;
    Uri uriFile = null;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);

    @Override
    public void onCreate() {
        super.onCreate();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        mDatabase = FirebaseDatabase.getInstance().getReference();
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);
        Bundle extras = intent.getExtras();
        if (extras.getString("nomTailleur") == null || extras.getString("nomTailleur").isEmpty()){
            more = "";
        }else {
            more = extras.getString("nomTailleur");
        }
        if (extras.getString("telTailleur") == null || extras.getString("telTailleur").isEmpty()){
            telTailleur = "";
        }else {
            telTailleur = extras.getString("telTailleur");
        }
        if (extras.getString("sexe") == null || extras.getString("sexe").isEmpty()){
            sexe = "";
        }else {
            sexe = extras.getString("sexe");
        }
        if (extras.getString("category") == null || extras.getString("category").isEmpty()){
            category = "";
        }else {
            category = extras.getString("category");
        }
        if (ACTION_UPLOAD.equals(intent.getAction())) {
            Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
            Uri fileUri1 = intent.getParcelableExtra(EXTRA_FILE_URI1);
            Uri fileUri2 = intent.getParcelableExtra(EXTRA_FILE_URI2);
            Uri fileUri3 = intent.getParcelableExtra(EXTRA_FILE_URI3);
            Uri fileUri4 = intent.getParcelableExtra(EXTRA_FILE_URI4);
            Uri fileUri5 = intent.getParcelableExtra(EXTRA_FILE_URI5);
            if (fileUri1 != null ){
                arrayList.add(fileUri1);
            }
            if (fileUri2 != null ){
                arrayList.add(fileUri2);
            }
            if (fileUri3 != null ){
                arrayList.add(fileUri3);
            }
            if (fileUri4 != null ){
                arrayList.add(fileUri4);
            }
            if (fileUri5 != null ){
                arrayList.add(fileUri5);
            }

            uploadFromUri(fileUri);
        }

        return START_REDELIVER_INTENT;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void compresImage(Uri fileUri){
        byte[] imageInByte = null;
        Bitmap b = null;
        try {
            b = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),fileUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = b;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length;
        if (lengthbmp > 300000){
            do {
                bitmap = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.5), (int)(bitmap.getHeight()*0.5), true);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                imageInByte = stream.toByteArray();
                lengthbmp = imageInByte.length;
            } while (lengthbmp < 300000);
            uriFile = getImageUri(getApplicationContext(), bitmap);
        }
    }

    private void uploadFromUri(final Uri fileUri) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());
        uriFile = fileUri;
        Runnable r = new Runnable() {
            public void run() {
                compresImage(fileUri);
                taskStarted();
                showUploadProgressNotification();
                final StorageReference photoRef = mStorageRef.child("photos")
                        .child(uriFile.getLastPathSegment());
                Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
                photoRef.putFile(uriFile)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Log.d(TAG, "uploadFromUri:onSuccess");

                                Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();

                                showUploadFinishedNotification(downloadUri, uriFile);
                                taskCompleted();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.w(TAG, "uploadFromUri:onFailure", exception);
                                broadcastUploadFinished(null, uriFile);
                                showUploadFinishedNotification(null, uriFile);
                                taskCompleted();
                            }
                        });
            }
        };

        new Thread(r).start();

    }

    private void uploadFromUriOneImage(final Uri fileUri) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());
        uriFile = fileUri;
        Runnable r = new Runnable() {
            public void run() {
                compresImage(fileUri);
                taskStarted();
                showUploadProgressNotification();
                final StorageReference photoRef = mStorageRef.child("photos")
                        .child(uriFile.getLastPathSegment());
                Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
                photoRef.putFile(uriFile)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Log.d(TAG, "uploadFromUri:onSuccess");

                                Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();

                                showUploadFinishedOneImage(downloadUri);
                                taskCompleted();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.w(TAG, "uploadFromUri:onFailure", exception);
                                broadcastUploadFinished(null, uriFile);
                                showUploadFinishedNotification(null, uriFile);
                                taskCompleted();
                            }
                        });
            }
        };

        new Thread(r).start();

    }

    /**
     * Broadcast finished upload (success or failure).
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastUploadFinished(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        boolean success = downloadUrl != null;

        String action = success ? UPLOAD_COMPLETED : UPLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri);
        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        Intent intent = new Intent(getApplicationContext(), ActivityMain.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* requestCode */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        boolean success = downloadUrl != null;
        String message = success ? "Photo enregistrée!" : "Echec de l'enregistrement!";
        int icon = success ? R.drawable.ic_audiotrack_dark : R.drawable.ic_audiotrack_dark;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
        final Uri uri = downloadUrl;
        postToPlayServer(getUserName(), uri.toString());
        Iterator<Uri> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            Uri element = iterator.next();
            uploadFromUriOneImage(element);
        }
    }

    private void showUploadFinishedOneImage(@Nullable Uri downloadUrl) {
        final Uri uri = downloadUrl;
        FirebaseDatabase.getInstance().getReference("users").child(getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);

                        if (user == null) {
                            Log.e(TAG, "User " + getUid() + " is unexpectedly null");
                            Toast.makeText(getApplicationContext(),
                                    "Error: Impossible de recuperer le user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            writeNewPostOneImage(uri.toString());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    private void postToPlayServer(String author, String url) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            Utilities.showMessage(null, getString(R.string.common_signin_button_text), getApplicationContext());
            return;
        }
        String pays = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry();
        PostItem postItem = new PostItem(author, getUid(), url, category, sexe, more, getPhotoUrl(), pays, telTailleur,"");
        Call<ResponseBase> operations = postService.doPost(postItem);
        operations.enqueue(callback);
    }

    private Callback<ResponseBase> callback = new Callback<ResponseBase>() {

        @Override
        public void onResponse(Call<ResponseBase> call, Response<ResponseBase> res) {
            int code = res.code();
            if (code == 200) {
                Toast.makeText(getApplicationContext(), "Modèle bien enregistrée.", Toast.LENGTH_SHORT).show();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }
            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.common_signin_button_text, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), "Erreur service", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<ResponseBase> call, Throwable t) {
            Toast.makeText(getApplicationContext(), "Post failed", Toast.LENGTH_SHORT).show();
        }
    };

    private void writeNewPostOneImage(String photo) {
        Map<String, String> userData = new HashMap<String, String>();
        //ca continue A ecraser photo1
        userData.put("photo1", photo);

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, userData);
        childUpdates.put("/user-posts/" + getUid() + "/" + key, userData);
        if (!category.isEmpty()){
            childUpdates.put("all" + category + "/" + key, userData);
            if (!sexe.isEmpty()){
                childUpdates.put(category + "/" + sexe + "/" + key, userData);
            }
        }

        FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates);
    }

    /**
     * Show notification with an indeterminate upload progress bar.
     */
    private void showUploadProgressNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_audiotrack_dark)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Enregistrement photo...")
                .setProgress(0, 0, true)
                .setOngoing(true)
                .setAutoCancel(false);

        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPLOAD_COMPLETED);
        filter.addAction(UPLOAD_ERROR);

        return filter;
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public String getPhotoUrl() {
        return FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString();
    }
    public String getUserName() {
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName().toString();
    }

}
