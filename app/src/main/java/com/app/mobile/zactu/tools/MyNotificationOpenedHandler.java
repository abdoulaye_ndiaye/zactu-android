package com.app.mobile.fashionable.tools;

import android.content.Intent;

import com.app.mobile.fashionable.ActivityMain;
import com.app.mobile.fashionable.ActivityNewsNotification;
import com.google.android.gms.ads.InterstitialAd;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by ghambyte on 07/03/2017.
 */

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        JSONObject data = result.notification.payload.additionalData;
        String url = (data != null) ? data.optString("id", null) : null;
        if (url != null) {
            final Intent browserIntent;

            if (url.contains("play.google.com")){
                browserIntent = new Intent(getApplicationContext(), ActivityMain.class);
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    browserIntent.putExtra(ActivityMain.ACCOUNT_SERVICE, result.notification.payload.additionalData.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getApplicationContext().startActivity(browserIntent);
            }
            else {
                browserIntent = new Intent(getApplicationContext(), ActivityNewsNotification.class);
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try {
                    browserIntent.putExtra(ActivityNewsNotification.EXTRA_OBJC, result.notification.payload.additionalData.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getApplicationContext().startActivity(browserIntent);
            }

        }

        else {
            Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        }
    }

}
